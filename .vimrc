colorscheme molokai

syntax enable " enable syntax processing

set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set expandtab       " tabs are spaces


set number              " show line numbers
set cursorline          " highlight current line
